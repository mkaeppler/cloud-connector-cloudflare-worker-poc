/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npm run dev` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npm run deploy` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

import auth from './auth';
import router from './router';

export default {
  async fetch(request, env, ctx) {
    if (!(await auth.verifyToken(request))) {
      return new Response('Access denied', { status: 401 });
    }

    return router.handle(request);
  },
};
