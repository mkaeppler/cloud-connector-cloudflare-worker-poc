import { Router } from 'itty-router';

const router = Router();

router.upstreamPath = function (req) {
  const full_path = new URL(req.url).pathname;
  const path_segments = full_path.split('/');
  path_segments.shift(); // pop empty element
  path_segments.shift(); // pop path prefix

  return path_segments.join('/');
}

router.defaultHttpHandler = async function (req, endpoint, extraHeaders) {
  //TODO: preserve query string
  const url = `${endpoint}/${router.upstreamPath(req)}`;

  const response = await fetch(url, {
    method: req.method,
    headers: {
      ...req.headers,
      ...extraHeaders
    },
    body: req.body
  });

  const { readable, writable } = new IdentityTransformStream();

  response.body.pipeTo(writable);

  return new Response(readable, response);
}

// Proxy AI requests
router.all('/ai/*', async (req) => {
  const endpoint = 'https://codesuggestions.gitlab.com';
  const token = req.headers.get('X-GitLab-Token');
  const headers = {
    'X-GitLab-Authentication-Type': 'oidc',
    'Authorization': `Bearer ${token}`
  };

  return await router.defaultHttpHandler(req, endpoint, headers);
});

// Fall through
router.all('*', () => new Response('Not Found', { status: 404 }));

export default router;
