// JWT auth

const auth = {
  verifyToken: async function(req) {
    var jose = require('jose');

    const token = req.headers.get('X-GitLab-Token');

    const resp = await fetch('https://gitlab.com/oauth/discovery/keys');
    const jwks = await resp.json();

    const alg = 'RS256';
    const options = {
      audience: 'gitlab-code-suggestions',
    }

    const verifiedToken = await jwks.keys.reduce(async (acc, jwk) => {
      if (acc) return acc;

      console.log(`Trying key: ${jwk.kid}`);
      const publicKey = await jose.importJWK(jwk, alg);
      return await jose.jwtVerify(token, publicKey, options).catch((err) => {
        console.log(`Verification failed: ${err}`);
      });
    }, null);

    return !!verifiedToken;
  }
}

export default auth;
